import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-alerta',
  templateUrl: './dialog-alerta.component.html',
  styleUrls: ['./dialog-alerta.component.css']
})
export class DialogAlertaComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogAlertaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { message: string }
  ) { }

  ngOnInit(): void {
  }

}
