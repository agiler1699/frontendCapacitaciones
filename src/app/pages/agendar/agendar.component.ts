import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { map } from 'rxjs/operators';
import { AdmAgendarService } from 'src/app/services/adm-agendar.service';
import { AdmClientesService } from 'src/app/services/adm-clientes.service';
import { AdmTemasService, ITema } from 'src/app/services/adm-temas.service';
import { DialogConfirmacionComponent } from 'src/app/shared/dialog-confirmacion/dialog-confirmacion.component';
import { ICliente } from '../adm-clientes/adm-clientes.component';

@Component({
  selector: 'app-agendar',
  templateUrl: './agendar.component.html',
  styleUrls: ['./agendar.component.css']
})
export class AgendarComponent implements OnInit {

  constructor(
    private clienteService: AdmClientesService,
    private temaService: AdmTemasService,
    private _fb: FormBuilder,
    private agendarService: AdmAgendarService,
    private dialog: MatDialog

  ) { }

  minDate: Date = new Date();

  public clientes: ICliente[] = [];
  public temas: ITema[] = [];
  public agendaForm: FormGroup = this._fb.group({})
  selected: boolean = false;
  nombre: String = '';
  plan_id: String = '';
  clienteSeleccionadoId: String = '';
  ngOnInit(): void {
    this.selected = false;
    this.clienteService.listarClientes()
      .subscribe(
        (data) => {
          this.clientes = data
        }
      );

  }
  get formArr() {
    return this.agendaForm.get('itemRows') as FormArray;
  }
  initAgendarRow(cliente_id: String, tema: String, nombreTema: String) {
    return this._fb.group({
      tema: tema,
      fecha: new FormControl(null, [Validators.required]),
      capacitador: new FormControl(null, [Validators.required]),
      personasCapacitar: new FormControl(null, [Validators.required]),
      descripcion: new FormControl(null, [Validators.required]),
      cliente: cliente_id,
      nombreTema: nombreTema
    });
  }
  async seleccionarCliente(nombre: String, plan_id: String, cliente_id: String): Promise<void> {
    this.nombre = nombre;
    this.plan_id = plan_id;
    await this.temaService.listarTemas(this.plan_id)
      .subscribe(
        (data) => {

          this.temas = data;
          this.agendaForm.addControl('itemRows', this._fb.array([]))

          this.temas.forEach(element => {
            console.log(element.nombre);
            this.formArr.push(this.initAgendarRow(cliente_id, element.id, element.nombre));
          });

          console.log(this.formArr.controls)
          this.clienteSeleccionadoId = cliente_id;
          this.selected = !this.selected;

        }
      )


  }


  onSubmit() {
    const dialogRef = this.dialog.open(DialogConfirmacionComponent, {

      data: { message: `Esta segur@ de que desea realizar la agenda?` }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.agendarService.crearAgenda(this.agendaForm.value).pipe(
          map(data => { })).subscribe();
        this.temas = [];
        this.agendaForm = this._fb.group({});
        this.clienteSeleccionadoId = '';
        this.selected = !this.selected;
      }
    });
  }


  cancelar() {
    this.temas = [];
    this.agendaForm = this._fb.group({});
    this.selected = !this.selected;
  }

}


