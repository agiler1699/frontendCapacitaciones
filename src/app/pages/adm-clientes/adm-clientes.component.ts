import { Component, OnInit } from '@angular/core';
import { AdmClientesService } from 'src/app/services/adm-clientes.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AdmPlanesService } from 'src/app/services/adm-planes.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { DialogConfirmacionComponent } from 'src/app/shared/dialog-confirmacion/dialog-confirmacion.component';

export interface ICliente {
  cliente_id: string;
  nombre: string;
  celular: string;
  email: string;
  direccion: string;
  plan: string;
  plan_id: string;
  estado: boolean,
}



@Component({
  selector: 'app-adm-clientes',
  templateUrl: './adm-clientes.component.html',
  styleUrls: ['./adm-clientes.component.css']
})
export class AdmClientesComponent implements OnInit {
  public clientes: ICliente[] = [];
  displayedColumns: string[] = ['index', 'nombre', 'celular', 'email', 'direccion', 'plan', 'editar', 'eliminar'];



  constructor(
    private admClientesService: AdmClientesService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.obtenerClientes();
  }
  obtenerClientes() {
    this.admClientesService.listarClientes()
      .subscribe(
        (data) => {
          console.log('datos en subs')
          console.log(data)
          this.clientes = data
          console.log('datos en subs planes')
          console.log(this.clientes)
        }
      );
  }
  editar(idCliente: number | String) {
    this.router.navigate(['/clientes/editar', idCliente])
  }

  openDialog(clienteNombre: string, idCliente: string): void {
    const dialogRef = this.dialog.open(DialogConfirmacionComponent, {
      data: { message: `Esta segur@ de que desea eliminar al cliente ${clienteNombre}` }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.admClientesService.eliminarCliente(idCliente).pipe(
          map(token => {
            this.obtenerClientes();
            this.router.navigate(['clientes'])
          })).subscribe();
      }
    });
  }





}
