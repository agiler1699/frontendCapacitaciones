import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AdmPlanesService } from 'src/app/services/adm-planes.service';
import { AdmTemasService } from 'src/app/services/adm-temas.service';

@Component({
  selector: 'app-adm-planes-editar',
  templateUrl: './adm-planes-editar.component.html',
  styleUrls: ['./adm-planes-editar.component.css']
})
export class AdmPlanesEditarComponent implements OnInit {
  constructor(
    private _fb: FormBuilder,
    private planService: AdmPlanesService,
    private router: Router,
    private route: ActivatedRoute,
    private temasService: AdmTemasService

  ) { }

  public planForm: FormGroup = new FormGroup({});
  name = new FormControl(null, [Validators.required]);
  idPlan: string = '';
  nombrePlan: string = '';
  temas = this._fb.array([]);

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.idPlan = String(params.get('id'));
      this.nombrePlan = String(params.get('nombrePlan'));
    })

    this.planForm.addControl('id', new FormControl(this.idPlan, [Validators.required]));
    this.planForm.addControl('subjects', this.temas)
    this.planForm.addControl('name', this.name);

    this.temasService.listarTemas(this.idPlan).subscribe(data => {
      this.name.setValue(this.nombrePlan);
      data.forEach(valores => {
        this.temas.push(this._fb.group({ 'id': new FormControl(valores.id, [Validators.required]), 'nombre': new FormControl(valores.nombre, [Validators.required]) },))
      })

    }
    )


  }
  get formArr() {
    return this.planForm.get('subjects') as FormArray;
  }

  initSubjects() {
    return this._fb.group({ 'id': 'nuevo', 'nombre': '' })
  }
  addNewTema() {
    this.formArr.push(this.initSubjects());
  }

  getErrorMessageName() {

    return 'Necesita ingresar el nombre del cliente';
  }

  getErrorMessageTema() {

    return 'Necesita ingresar el nombre del tema';
  }
  onSubmit() {
    if (this.planForm.invalid) {
      return;
    }
    this.planService.actualizarPlan(this.planForm.value).pipe(
      map(token => this.router.navigate(['planes']))).subscribe();

  }
}
