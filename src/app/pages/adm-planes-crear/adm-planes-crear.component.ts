import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AdmPlanesService } from 'src/app/services/adm-planes.service';
import { DialogAlertaComponent } from 'src/app/shared/dialog-alerta/dialog-alerta.component';

@Component({
  selector: 'app-adm-planes-crear',
  templateUrl: './adm-planes-crear.component.html',
  styleUrls: ['./adm-planes-crear.component.css']
})
export class AdmPlanesCrearComponent implements OnInit {
  constructor(
    private _fb: FormBuilder,
    private planService: AdmPlanesService,
    private router: Router,
    private dialog: MatDialog


  ) { }

  public planForm: FormGroup = new FormGroup({});
  name = new FormControl(null, [Validators.required]);


  ngOnInit(): void {
    this.planForm.addControl('subjects', this._fb.array([this.initSubjects()]))
    this.planForm.addControl('name', this.name);

  }
  get formArr() {
    return this.planForm.get('subjects') as FormArray;
  }

  initSubjects() {
    return this._fb.control(null, [Validators.required])
  }
  addNewTema() {
    this.formArr.push(this.initSubjects());
  }

  getErrorMessageName() {

    return 'Necesita ingresar el nombre del cliente';
  }

  getErrorMessageTema() {

    return 'Necesita ingresar el nombre del tema';
  }
  onSubmit() {

    if (this.planForm.invalid) {
      return;
    }
    this.planService.crearPlan(this.planForm.value).subscribe(data => {
      const dialogRef = this.dialog.open(DialogAlertaComponent, {

        data: { message: 'Plan creado correctamente' }
      });
      this.router.navigate(['administracion'])
    }, error => {
      const dialogRef = this.dialog.open(DialogAlertaComponent, {

        data: { message: error.error.message }
      });
    });


  }
}
