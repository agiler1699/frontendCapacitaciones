import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AdmClientesService } from 'src/app/services/adm-clientes.service';
import { AdmPlanesService } from 'src/app/services/adm-planes.service';
import { ICliente } from '../adm-clientes/adm-clientes.component';
import { IPlan } from '../adm-planes/adm-planes.component';

@Component({
  selector: 'app-adm-clientes-editar',
  templateUrl: './adm-clientes-editar.component.html',
  styleUrls: ['./adm-clientes-editar.component.css']
})
export class AdmClientesEditarComponent implements OnInit {

  constructor(private route: ActivatedRoute, private _fb: FormBuilder,
    private router: Router,
    private planService: AdmPlanesService,
    private clienteService: AdmClientesService) { }
  idCliente: string = '';


  public clienteForm: FormGroup = new FormGroup({});
  name = new FormControl(null, [Validators.required]);
  phone = new FormControl(null, [Validators.required, Validators.pattern('^[0-9\-\+]{9,15}$')]);
  email = new FormControl(null, [Validators.required, Validators.email]);
  address = new FormControl(null, [Validators.required]);
  plan = new FormControl(null, [Validators.required]);

  public planes: IPlan[] = [];
  public cliente: ICliente = {
    cliente_id: '',
    nombre: '',
    celular: '',
    email: '',
    direccion: '',
    plan: '',
    plan_id: '',
    estado: true,
  };

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.idCliente = String(params.get('id'));
    })
    this.clienteForm.addControl('id', new FormControl(this.idCliente, [Validators.required]));
    this.clienteForm.addControl('name', this.name);
    this.clienteForm.addControl('phone', this.phone);
    this.clienteForm.addControl('email', this.email);
    this.clienteForm.addControl('address', this.address);
    this.clienteForm.addControl('plan', this.plan);
    this.clienteService.obtenerCliente(this.idCliente).subscribe(data => {
      this.cliente = data;
      this.name.setValue(this.cliente.nombre);
      this.phone.setValue(this.cliente.celular);
      this.email.setValue(this.cliente.email);
      this.address.setValue(this.cliente.direccion);
      this.plan.setValue(this.cliente.plan_id);


      this.planService.listarPlanes()
        .subscribe(
          (data) => {
            this.planes = data
          }
        );
    });




  }
  getErrorMessageEmail() {

    return 'Necesita ingresar el email del cliente';
  }

  getErrorMessagePhone() {
    if (this.clienteForm.get('phone')?.hasError('required'))
      return 'Necesita ingresar el numero de telefono';
    return this.clienteForm.get('phone')?.hasError('pattern') ? 'No es un telefono valido' : '';

  }
  getErrorMessageName() {

    return 'Necesita ingresar el nombre del cliente';
  }

  onSubmit() {
    if (this.clienteForm.invalid) {
      return;
    }
    this.clienteService.actualizarCliente(this.clienteForm.value).pipe(
      map(token => this.router.navigate(['clientes']))).subscribe();
  }


}


