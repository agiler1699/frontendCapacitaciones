import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages/pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdministracionComponent } from './administracion/administracion.component';
import { AdmClientesComponent } from './adm-clientes/adm-clientes.component';
import { AdmPlanesComponent } from './adm-planes/adm-planes.component';
import { AdmClientesCrearComponent } from './adm-clientes-crear/adm-clientes-crear.component';
import { AdmPlanesCrearComponent } from './adm-planes-crear/adm-planes-crear.component';
import { AuthGuard } from '../guards/auth.guard';
import { AgendarComponent } from './agendar/agendar.component';
import { AjustesComponent } from './ajustes/ajustes.component';
import { AdmClientesEditarComponent } from './adm-clientes-editar/adm-clientes-editar.component';
import { AdmPlanesEditarComponent } from './adm-planes-editar/adm-planes-editar.component';
import { AgendasClienteComponent } from './agendas-cliente/agendas-cliente.component';
import { ComunicacionesComponent } from './comunicaciones/comunicaciones.component';
import { ComunicacionesClienteComponent } from './comunicaciones-cliente/comunicaciones-cliente.component';



const routes: Routes = [

    {
        path: '',
        component: PagesComponent,
        canActivate: [AuthGuard],
        children:
            [
                { path: 'dashboard', component: DashboardComponent },
                { path: 'administracion', component: AdministracionComponent },
                { path: 'clientes', component: AdmClientesComponent },
                { path: 'clientes/crear', component: AdmClientesCrearComponent },
                { path: 'planes', component: AdmPlanesComponent },
                { path: 'planes/crear', component: AdmPlanesCrearComponent },
                { path: 'agendar', component: AgendarComponent },
                { path: 'comunicaciones', component: ComunicacionesComponent },
                { path: 'ajustes', component: AjustesComponent },
                { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
                { path: 'clientes/editar/:id', component: AdmClientesEditarComponent },
                { path: 'planes/editar/:id/:nombrePlan', component: AdmPlanesEditarComponent },
                { path: 'agendas/cliente/:nombre/:id', component: AgendasClienteComponent },
                { path: 'comunicaciones/cliente/:nombre/:id', component: ComunicacionesClienteComponent }

            ]
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
