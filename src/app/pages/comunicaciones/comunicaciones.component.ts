import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { map } from 'rxjs/operators';
import { AdmAgendarService } from 'src/app/services/adm-agendar.service';
import { AdmClientesService } from 'src/app/services/adm-clientes.service';
import { AdmComunicacionService } from 'src/app/services/adm-comunicacion.service';
import { AdmTemasService, ITema } from 'src/app/services/adm-temas.service';
import { DialogConfirmacionComponent } from 'src/app/shared/dialog-confirmacion/dialog-confirmacion.component';
import { ICliente } from '../adm-clientes/adm-clientes.component';

@Component({
  selector: 'app-comunicaciones',
  templateUrl: './comunicaciones.component.html',
  styleUrls: ['./comunicaciones.component.css']
})
export class ComunicacionesComponent implements OnInit {

  constructor(
    private clienteService: AdmClientesService,
    private _fb: FormBuilder,
    private comunicacionService: AdmComunicacionService,
    private dialog: MatDialog

  ) { }



  public clientes: ICliente[] = [];
  public comunicacionForm: FormGroup = this._fb.group({})
  public texto: FormControl = new FormControl(null, [Validators.required]);
  public clienteId: FormControl = new FormControl(null, [Validators.required]);
  selected: boolean = false;
  nombre: String = '';
  clienteSeleccionadoId: String = '';

  ngOnInit(): void {
    this.selected = false;
    this.clienteService.listarClientes()
      .subscribe(
        (data) => {
          this.clientes = data
        }
      );

  }


  async seleccionarCliente(nombre: String, plan_id: String, cliente_id: String): Promise<void> {
    this.nombre = nombre;
    this.clienteSeleccionadoId = cliente_id;
    this.clienteId.setValue(cliente_id);
    this.comunicacionForm.addControl('texto', this.texto);
    this.comunicacionForm.addControl('id', this.clienteId);
    this.selected = !this.selected;
  }


  onSubmit() {
    const dialogRef = this.dialog.open(DialogConfirmacionComponent, {

      data: { message: `Esta segur@ de que desea realizar la comunicación?` }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.comunicacionService.crearComunicacion(this.comunicacionForm.value.id, this.comunicacionForm.value.texto).pipe(
          map(data => { })).subscribe();
        this.comunicacionForm = this._fb.group({});
        this.texto = new FormControl(null, [Validators.required]);
        this.clienteId = new FormControl(null, [Validators.required]);
        this.clienteSeleccionadoId = '';
        this.selected = !this.selected;
      }
    });
  }


  cancelar() {
    this.comunicacionForm = this._fb.group({});
    this.texto = new FormControl(null, [Validators.required]);
    this.clienteId = new FormControl(null, [Validators.required]);
    this.selected = !this.selected;
  }

}
