import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AdmPlanesService } from 'src/app/services/adm-planes.service';

@Component({
  selector: 'app-administracion',
  templateUrl: './administracion.component.html',
  styleUrls: ['./administracion.component.css']
})
export class AdministracionComponent implements OnInit {

  constructor(
    private admPlanesService: AdmPlanesService,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

}
