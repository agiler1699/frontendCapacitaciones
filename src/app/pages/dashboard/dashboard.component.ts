import { Component, OnInit } from '@angular/core';
import { AdmUsuarioService, IUsuario } from 'src/app/services/adm-usuario.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private usuarioService: AdmUsuarioService) { }

  usuario: IUsuario = {
    company: '',
    email: '',
    ruc: ''
  }
  ngOnInit(): void {
    this.usuarioService.obtenerUsuario().subscribe(data => this.usuario = data)
  }

}
