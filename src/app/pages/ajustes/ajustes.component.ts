import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AdmUsuarioService } from 'src/app/services/adm-usuario.service';
import { DialogAlertaComponent } from 'src/app/shared/dialog-alerta/dialog-alerta.component';

@Component({
  selector: 'app-ajustes',
  templateUrl: './ajustes.component.html',
  styleUrls: ['./ajustes.component.css']
})
export class AjustesComponent implements OnInit {

  constructor(private router: Router, private usuarioService: AdmUsuarioService,
    private dialog: MatDialog
  ) { }

  public companiaForm: FormGroup = new FormGroup({ tipo: new FormControl('company') });
  nombreCompania = new FormControl(null, [Validators.required]);

  public rucForm: FormGroup = new FormGroup({ tipo: new FormControl('ruc') });
  ruc = new FormControl(null, [
    Validators.required,
    Validators.pattern(/^[0-9]\d*$/),
    Validators.minLength(13),
    Validators.maxLength(13),
  ]);

  public emailForm: FormGroup = new FormGroup({ tipo: new FormControl('email') });
  email = new FormControl(null, [Validators.required, Validators.email]);

  public passForm: FormGroup = new FormGroup({ tipo: new FormControl('password') });
  passwordActual = new FormControl(null, [
    Validators.required,
  ]);
  passwordNueva = new FormControl(null, [
    Validators.required,
  ]);

  ngOnInit(): void {
    this.companiaForm.addControl('nuevo', this.nombreCompania);
    this.rucForm.addControl('nuevo', this.ruc);
    this.emailForm.addControl('nuevo', this.email);
    this.passForm.addControl('actual', this.passwordActual);
    this.passForm.addControl('nuevo', this.passwordNueva);

    this.usuarioService.obtenerUsuario().subscribe(data => {
      this.nombreCompania.setValue(data.company);
      this.ruc.setValue(data.ruc);
      this.email.setValue(data.email);
    })
  };
  getErrorMessageEmail() {

    if (this.emailForm.get('nuevo')?.hasError('required')) {
      return 'Necesita ingresar el correo';
    }

    return this.emailForm.get('nuevo')?.hasError('email') ? 'No es un correo valido' : '';
  }
  getErrorMessagePassword() {

    return 'Necesita ingresar la contrasena';

  }
  getErrorMessageCompanyName() {

    return 'Necesita ingresar el nombre de su empresa';


  }
  getErrorMessageRuc() {

    if (this.rucForm.get('nuevo')?.hasError('required')) {
      return 'Necesita ingresar el RUC';
    }
    if (this.rucForm.get('nuevo')?.hasError('minlength')) {
      return 'El RUC tiene menos de 13 caracteres';
    }
    if (this.rucForm.get('nuevo')?.hasError('maxlength')) {
      return 'El RUC tiene mas de 13 caracteres';
    }
    return this.rucForm.get('nuevo')?.hasError('pattern') ? 'No es un RUC valido' : '';
  }
  onSubmitCompania() {
    this.usuarioService.actualizarUsuario(this.companiaForm.value).subscribe(
      (data => {
        const dialogRef = this.dialog.open(DialogAlertaComponent, {
          data: { message: 'Nombre de la compañía cambiado con éxito' }
        });
        localStorage.clear();
        this.router.navigate(['/login'])
      })
    )
  }
  onSubmitEmail() {
    this.usuarioService.actualizarUsuario(this.emailForm.value).subscribe(
      (data => {
        const dialogRef = this.dialog.open(DialogAlertaComponent, {
          data: { message: 'Email cambiado con éxito' }
        });
        localStorage.clear();
        this.router.navigate(['/login'])
      })
    )

  }
  onSubmitRuc() {
    this.usuarioService.actualizarUsuario(this.rucForm.value).subscribe(
      (data => {
        const dialogRef = this.dialog.open(DialogAlertaComponent, {
          data: { message: 'RUC cambiado con éxito' }
        });
        localStorage.clear();
        this.router.navigate(['/login'])
      })
    )

  }
  onSubmitPassword() {

    this.usuarioService.actualizarUsuario(this.passForm.value).subscribe(
      (data => {
        const dialogRef = this.dialog.open(DialogAlertaComponent, {
          data: { message: 'Contraseña cambiada con éxito' }
        });
        localStorage.clear();
        this.router.navigate(['/login'])
      }), error => {

        const dialogRef = this.dialog.open(DialogAlertaComponent, {
          data: { message: error.error.message }
        });

      }
    )

  }


}
