import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AdmPlanesService } from 'src/app/services/adm-planes.service';

import { MatTableDataSource } from '@angular/material/table';
import { DataSource } from '@angular/cdk/table';
import { MatDialog } from '@angular/material/dialog';
import { DialogConfirmacionComponent } from 'src/app/shared/dialog-confirmacion/dialog-confirmacion.component';

export interface IPlan {
  id: number;
  nombre: string
};
@Component({
  selector: 'app-adm-planes',
  templateUrl: './adm-planes.component.html',
  styleUrls: ['./adm-planes.component.css']
})

export class AdmPlanesComponent implements OnInit {

  public planes: IPlan[] = [];
  dataSource: any;

  displayedColumns: string[] = ['index', 'nombre', 'editar', 'eliminar'];

  constructor(
    private admPlanesService: AdmPlanesService,
    private router: Router,
    private dialog: MatDialog

  ) {

  }


  async ngOnInit() {
    this.obtenerPlanes()

  }

  obtenerPlanes() {
    this.admPlanesService.listarPlanes()
      .subscribe(
        (data) => {
          console.log('datos en subs')
          console.log(data)
          this.planes = data
          console.log('datos en subs planes')
          console.log(this.planes)
          this.dataSource = new MatTableDataSource(this.planes);
        }
      );
  }

  editar(idPlan: String, nombrePlan: String) {
    this.router.navigate(['/planes/editar', idPlan, nombrePlan])
  }

  openDialog(planNombre: string, idPlan: String): void {
    const dialogRef = this.dialog.open(DialogConfirmacionComponent, {

      data: { message: `Esta segur@ de que desea eliminar el plan ${planNombre}` }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.admPlanesService.eliminarPlan(idPlan).pipe(
          map(token => {
            this.obtenerPlanes();
            this.router.navigate(['planes'])
          })).subscribe();
      }
    });
  }

}
