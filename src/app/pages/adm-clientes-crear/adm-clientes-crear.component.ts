import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AdmClientesService } from 'src/app/services/adm-clientes.service';
import { AdmPlanesService } from 'src/app/services/adm-planes.service';
import { DialogAlertaComponent } from 'src/app/shared/dialog-alerta/dialog-alerta.component';
import { IPlan } from '../adm-planes/adm-planes.component';

@Component({
  selector: 'app-adm-clientes-crear',
  templateUrl: './adm-clientes-crear.component.html',
  styleUrls: ['./adm-clientes-crear.component.css']
})
export class AdmClientesCrearComponent implements OnInit {

  constructor(
    private _fb: FormBuilder,
    private router: Router,
    private planService: AdmPlanesService,
    private clienteService: AdmClientesService,
    private dialog: MatDialog

  ) { }

  public clienteForm: FormGroup = new FormGroup({});
  name = new FormControl(null, [Validators.required]);
  phone = new FormControl(null, [Validators.required, Validators.pattern('^[0-9\-\+]{9,15}$')]);
  email = new FormControl(null, [Validators.required, Validators.email]);
  address = new FormControl(null, [Validators.required]);
  plan = new FormControl(null, [Validators.required]);

  public planes: IPlan[] = [];

  ngOnInit(): void {
    this.clienteForm.addControl('name', this.name);
    this.clienteForm.addControl('phone', this.phone);
    this.clienteForm.addControl('email', this.email);
    this.clienteForm.addControl('address', this.address);
    this.clienteForm.addControl('plan', this.plan);

    this.planService.listarPlanes()
      .subscribe(
        (data) => {
          this.planes = data
        }
      );

  }
  getErrorMessageEmail() {

    return 'Necesita ingresar el email del cliente';
  }

  getErrorMessagePhone() {
    if (this.clienteForm.get('phone')?.hasError('required'))
      return 'Necesita ingresar el numero de telefono';
    return this.clienteForm.get('phone')?.hasError('pattern') ? 'No es un telefono valido' : '';

  }
  getErrorMessageName() {

    return 'Necesita ingresar el nombre del cliente';
  }

  onSubmit() {
    if (this.clienteForm.invalid) {
      return;
    }
    this.clienteService.crearCliente(this.clienteForm.value).subscribe(data => {
      const dialogRef = this.dialog.open(DialogAlertaComponent, {

        data: { message: 'Cliente creado correctamente' }
      });
      this.router.navigate(['administracion'])
    }, error => {
      const dialogRef = this.dialog.open(DialogAlertaComponent, {

        data: { message: error.error.message }
      });
    });
  }
}
