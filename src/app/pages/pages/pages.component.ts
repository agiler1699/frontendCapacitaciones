import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DialogConfirmacionComponent } from 'src/app/shared/dialog-confirmacion/dialog-confirmacion.component';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {

  constructor(private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(

  ): void {
  }

  logout(): void {
    const dialogRef = this.dialog.open(DialogConfirmacionComponent, {
      data: { message: `Esta segur@ de que desea cerrar sesión` }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        localStorage.removeItem('token');
        this.router.navigate(['/login']);

      }
    });

  }
}
