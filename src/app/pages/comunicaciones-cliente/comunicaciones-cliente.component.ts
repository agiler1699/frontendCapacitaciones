import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdmComunicacionService, IComunicacionLista } from 'src/app/services/adm-comunicacion.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-comunicaciones-cliente',
  templateUrl: './comunicaciones-cliente.component.html',
  styleUrls: ['./comunicaciones-cliente.component.css']
})
export class ComunicacionesClienteComponent implements OnInit {

  constructor(private route: ActivatedRoute, private comnucacionesService: AdmComunicacionService,
    private _location: Location) { }

  clienteId: string = '';
  clienteNombre: string = '';
  comunicaciones: IComunicacionLista[] = []
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.clienteId = String(params.get('id'));
      this.clienteNombre = String(params.get('nombre'));
    })
    this.obtenerComunicaciones();
  }
  obtenerComunicaciones() {

    this.comnucacionesService.listarComunicacion(this.clienteId).subscribe(data => {
      this.comunicaciones = data;
      console.log('datos dentro de ', data)

    })
  }
  regresar() {
    this._location.back();
  }


}
