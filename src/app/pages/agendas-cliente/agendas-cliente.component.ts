import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { AdmAgendarService, IAgendas } from 'src/app/services/adm-agendar.service';

@Component({
  selector: 'app-agendas-cliente',
  templateUrl: './agendas-cliente.component.html',
  styleUrls: ['./agendas-cliente.component.css']
})
export class AgendasClienteComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private agendasService: AdmAgendarService,
    private _location: Location) { }
  agendas: IAgendas[] = [];
  clienteId: string = '';
  clienteNombre: string = '';
  displayedColumns: string[] = ['index', 'tema', 'fechaCapacitacion', 'capacitador', 'personasCapacitar', 'descripcion'];

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.clienteId = String(params.get('id'));
      this.clienteNombre = String(params.get('nombre'));
    })
    this.obtenerAgendas();
  }
  obtenerAgendas() {
    this.agendasService.obtenerAgendasCliente(this.clienteId).subscribe(data => {
      this.agendas = data;
      console.log(this.agendas);
    })
  }
  regresar() {
    this._location.back();
  }

}
