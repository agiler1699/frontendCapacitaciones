import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NopagefoundComponent } from './pages/nopagefound/nopagefound.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { HeaderComponent } from './shared/header/header.component';
import { AdministracionComponent } from './pages/administracion/administracion.component';
import { PagesComponent } from './pages/pages/pages.component';
import { MatCardModule } from '@angular/material/card';

import { MatButtonModule } from '@angular/material/button';
import { AdmClientesComponent } from './pages/adm-clientes/adm-clientes.component';
import { AdmPlanesComponent } from './pages/adm-planes/adm-planes.component'
import { FlexLayoutModule } from '@angular/flex-layout';
import { AuthInterceptorService } from './services/auth-interceptor.service';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';


import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';


import { MatTableModule } from '@angular/material/table';
import { AdmPlanesCrearComponent } from './pages/adm-planes-crear/adm-planes-crear.component';
import { AdmClientesCrearComponent } from './pages/adm-clientes-crear/adm-clientes-crear.component';

import { JwtModule } from "@auth0/angular-jwt";
import { AgendarComponent } from './pages/agendar/agendar.component';
import { AjustesComponent } from './pages/ajustes/ajustes.component';
import { AdmClientesEditarComponent } from './pages/adm-clientes-editar/adm-clientes-editar.component';
import { AdmPlanesEditarComponent } from './pages/adm-planes-editar/adm-planes-editar.component';
import { AgendasClienteComponent } from './pages/agendas-cliente/agendas-cliente.component';
import { DialogConfirmacionComponent } from './shared/dialog-confirmacion/dialog-confirmacion.component';
import { DialogAlertaComponent } from './shared/dialog-alerta/dialog-alerta.component';
import { ComunicacionesComponent } from './pages/comunicaciones/comunicaciones.component';
import { ComunicacionesClienteComponent } from './pages/comunicaciones-cliente/comunicaciones-cliente.component';
export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NopagefoundComponent,
    SidebarComponent,
    HeaderComponent,
    AdministracionComponent,
    PagesComponent,
    AdmClientesComponent,
    AdmPlanesComponent,
    AdmPlanesCrearComponent,
    AdmClientesCrearComponent,
    AgendarComponent,
    AjustesComponent,
    AdmClientesEditarComponent,
    AdmPlanesEditarComponent,
    AgendasClienteComponent,
    DialogConfirmacionComponent,
    DialogAlertaComponent,
    ComunicacionesComponent,
    ComunicacionesClienteComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    FlexLayoutModule,
    MatTableModule,
    MatSelectModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["34.123.117.217:8088", '*'],
        disallowedRoutes: [""],
      },
    }),

  ],
  providers: [
    MatDatepickerModule,
    MatNativeDateModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
