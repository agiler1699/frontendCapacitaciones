import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ICliente } from '../pages/adm-clientes/adm-clientes.component';
import { environment } from 'src/environments/environment';


export interface ClienteForm {
  name: String;
  email: String;
  address: String;
  phone: String;
  plan: String;
}
export interface actualizarClienteForm {
  id: String;
  name: String;
  email: String;
  address: String;
  phone: String;
  plan: String;
}


@Injectable({
  providedIn: 'root'
})
export class AdmClientesService {

  constructor(private http: HttpClient) { }
  listarClientes(): Observable<ICliente[]> {
    return this.http.get<ICliente[]>(environment.apiUrl + 'cliente/listar').pipe(
      map((data) => {
        return data;
      })
    )
  }

  crearCliente(clienteForm: ClienteForm) {

    console.log(clienteForm)

    return this.http.post<any>(environment.apiUrl + 'cliente/crear',
      {
        name: clienteForm.name,
        email: clienteForm.email,
        address: clienteForm.address,
        phone: clienteForm.phone,
        plan: clienteForm.plan
      }).pipe(
        map((respuesta) => {
          return respuesta.message;
        })
      )

  }
  actualizarCliente(clienteForm: actualizarClienteForm) {
    return this.http.post<any>(environment.apiUrl + 'cliente/actualizar',
      {
        id: clienteForm.id,
        name: clienteForm.name,
        email: clienteForm.email,
        address: clienteForm.address,
        phone: clienteForm.phone,
        plan: clienteForm.plan
      }).pipe(
        map((respuesta) => {
          return respuesta.message;
        })
      )
  }

  obtenerCliente(clienteId: string): Observable<ICliente> {


    return this.http.post<ICliente>(environment.apiUrl + 'cliente/obtener',
      {
        clienteId: clienteId
      }).pipe(
        map((data) => {
          return data;
        })
      )

  }
  eliminarCliente(clienteId: string) {
    return this.http.post<ICliente>(environment.apiUrl + 'cliente/eliminar',
      {
        id: clienteId
      }).pipe(
        map((data) => {
          return data;
        })
      )

  }

}
