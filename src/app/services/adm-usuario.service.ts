import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

export interface ActualizarUsuarioForm {
    tipo: String;
    nuevo: String;
    actual: string;
}

export interface IUsuario {
    company: string;
    ruc: string;
    email: string;
}


@Injectable({
    providedIn: 'root'
})
export class AdmUsuarioService {

    constructor(private http: HttpClient) { }

    actualizarUsuario(actuliazarForm: ActualizarUsuarioForm) {
        if (actuliazarForm.tipo == 'password') {
            return this.http.post<any>(environment.apiUrl + 'usuario/actualizar',
                {
                    tipo: actuliazarForm.tipo,
                    nuevo: actuliazarForm.nuevo,
                    actual: actuliazarForm.actual
                }).pipe(
                    map((respuesta) => {
                        return respuesta.message;
                    })
                )
        } else

            return this.http.post<any>(environment.apiUrl + 'usuario/actualizar',
                {
                    tipo: actuliazarForm.tipo,
                    nuevo: actuliazarForm.nuevo
                }).pipe(
                    map((respuesta) => {
                        return respuesta.message;
                    })
                )
    }
    obtenerUsuario(): Observable<IUsuario> {
        return this.http.get<IUsuario>(environment.apiUrl + 'usuario/obtener').pipe(
            map(data => { return data; }));
    }


}
