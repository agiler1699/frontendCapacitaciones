import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

export interface IComunicacionLista {
  texto: string;
}

@Injectable({
  providedIn: 'root'
})

export class AdmComunicacionService {

  constructor(private http: HttpClient) { }

  crearComunicacion(clienteId: string, texto: string) {
    console.log('cliente id ', clienteId)
    console.log('texto ', texto)
    return this.http.post<any>(environment.apiUrl + 'comunicacion/crear',
      {
        idCliente: clienteId,
        texto: texto
      }).pipe(
        map((respuesta) => {
          return respuesta.message;
        })
      )

  }
  listarComunicacion(clienteId: string): Observable<IComunicacionLista[]> {
    return this.http.post<IComunicacionLista[]>(environment.apiUrl + 'comunicacion/listar',
      {
        idCliente: clienteId,
      }).pipe(
        map((respuesta) => {
          return respuesta;
        })
      )

  }




}
