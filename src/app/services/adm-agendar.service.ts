import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

export interface AgendarForm {
  tema: String;
  fecha: String;
  capacitador: String;
  personasCapacitar: String;
  descripcion: String;
  cliente: String;
  nombreTema: String;
}

export interface IAgendas {
  fecha: String;
  capacitador: String;
  personasCapacitar: String;
  descripcion: String;
  nombreTema: String;
}

@Injectable({
  providedIn: 'root'
})
export class AdmAgendarService {

  constructor(private http: HttpClient) { }

  crearAgenda(agendarForm: AgendarForm[]) {

    return this.http.post<any>(environment.apiUrl + 'agenda/crear',
      {
        formulario: agendarForm
      }).pipe(
        map((respuesta) => {
          return respuesta.message;
        })
      )

  }
  obtenerAgendasCliente(idCliente: string): Observable<IAgendas[]> {
    return this.http.post<IAgendas[]>(environment.apiUrl + 'agenda/obtener/cliente', {
      idCliente: idCliente
    }).pipe(
      map((response) => {
        return response;
      }))
  }


}
