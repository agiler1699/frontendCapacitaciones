import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

export interface ITema {
  id: String;
  nombre: String;
}

@Injectable({
  providedIn: 'root'
})
export class AdmTemasService {

  constructor(private http: HttpClient) { }

  listarTemas(plan_id: String): Observable<ITema[]> {
    console.log('plan id: ' + `${plan_id}`)
    return this.http.post<ITema[]>(environment.apiUrl + 'tema/listar', {

      'plan_id': `${plan_id}`

    }).pipe(
      map((data) => {
        return data;
      })
    )
  }

}
