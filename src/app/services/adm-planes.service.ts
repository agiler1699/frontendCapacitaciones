import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { IPlan } from '../pages/adm-planes/adm-planes.component';

export interface PlanForm {
  name: String;
  subjects: Array<String>;
}

export interface actualizarPlanForm {
  id: String;
  name: String;
  subjects: Array<JSON>;
}

@Injectable({
  providedIn: 'root'
})


export class AdmPlanesService {

  constructor(private http: HttpClient) { }

  listarPlanes(): Observable<IPlan[]> {
    return this.http.get<IPlan[]>(environment.apiUrl + 'plan/listar').pipe(
      map((data) => {
        return data;
      })
    )
  }

  eliminarPlan(idPlan: String) {
    return this.http.post<any>(environment.apiUrl + 'plan/eliminar', { id: idPlan }).pipe(
      map((respuesta) => {
        return respuesta.message;
      })
    )
  }

  actualizarPlan(planForm: actualizarPlanForm) {
    return this.http.post<any>(environment.apiUrl + 'plan/actualizar', { id: planForm.id, name: planForm.name, subjects: planForm.subjects }).pipe(
      map((respuesta) => {
        return respuesta.message;
      })
    )

  }

  crearPlan(planForm: PlanForm) {
    console.log(
      planForm.name
    );
    console.log(
      planForm.subjects
    )
    return this.http.post<any>(environment.apiUrl + 'plan/crear', { name: planForm.name, subjects: planForm.subjects }).pipe(
      map((respuesta) => {
        return respuesta.message;
      })
    )

  }

}
