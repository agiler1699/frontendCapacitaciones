import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';
export interface LoginForm {
  email: string;
  password: string;
}
export interface RegisterForm {
  email: string;
  password: string;
  ruc: string;
  companyName: string;
}
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient, public jwtHelper: JwtHelperService) { }


  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    // Check whether the token is expired and return
    // true or false
    if (token) {
      return !this.jwtHelper.isTokenExpired(token);
    }
    return false;
  }


  login(loginForm: LoginForm) {
    return this.http.post<any>(environment.apiUrl + 'usuario/login', { email: loginForm.email, password: loginForm.password }).pipe(
      map((token) => {
        localStorage.setItem('token', token.sesionToken);
        return token;
      })
    )
  }
  register(registerForm: RegisterForm) {
    return this.http.post<any>(environment.apiUrl + 'usuario/crear', { email: registerForm.email, password: registerForm.password, ruc: registerForm.ruc, companyName: registerForm.companyName }).pipe(
      map((respuesta) => {
        return respuesta.message;
      })
    )
  }

}
