import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})

export class NoAuthGuard implements CanActivate {
  constructor(public auth: AuthenticationService, public router: Router) { }
  canActivate(): boolean {
    console.log('entro a not auth guard')
    if (this.auth.isAuthenticated()) {
      console.log('Esta autenticado')
      this.router.navigate(['/dashboard']);
      return false;
    } else
      return true;
  }

}
