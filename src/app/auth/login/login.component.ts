import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { DialogAlertaComponent } from 'src/app/shared/dialog-alerta/dialog-alerta.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({});
  email = new FormControl(null, [Validators.required, Validators.email]);
  password = new FormControl(null, [
    Validators.required,
  ]);

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    localStorage.clear();
    this.loginForm.addControl('email', this.email);
    this.loginForm.addControl('password', this.password);

  }
  getErrorMessageEmail() {


    if (this.loginForm.get('email')?.hasError('required')) {
      return 'Necesita ingresar el correo';
    }

    return this.loginForm.get('email')?.hasError('email') ? 'No es un correo valido' : '';
  }
  getErrorMessagePassword() {

    return 'Necesita ingresar la contrasena';


  }
  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    this.authService.login(this.loginForm.value).subscribe((data) => {

      return this.router.navigate(['dashboard'])
    }, error => {
      const dialogRef = this.dialog.open(DialogAlertaComponent, {
        data: { message: error.error.message }
      });
    });

  }


}
