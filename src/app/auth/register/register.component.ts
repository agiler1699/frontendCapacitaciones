import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DialogAlertaComponent } from 'src/app/shared/dialog-alerta/dialog-alerta.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  registerForm: FormGroup = new FormGroup({});
  email = new FormControl(null, [Validators.required, Validators.email]);
  password = new FormControl(null, [
    Validators.required,
  ]);
  companyName = new FormControl(null, [
    Validators.required,
  ]);
  ruc = new FormControl(null, [
    Validators.required,
    Validators.pattern(/^[0-9]\d*$/),
    Validators.minLength(13),
    Validators.maxLength(13),
  ]);

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.registerForm.addControl('email', this.email);
    this.registerForm.addControl('password', this.password);
    this.registerForm.addControl('companyName', this.companyName);
    this.registerForm.addControl('ruc', this.ruc);


  }
  getErrorMessageEmail() {


    if (this.registerForm.get('email')?.hasError('required')) {
      return 'Necesita ingresar el correo';
    }

    return this.registerForm.get('email')?.hasError('email') ? 'No es un correo valido' : '';
  }
  getErrorMessagePassword() {

    return 'Necesita ingresar la contrasena';


  }
  getErrorMessageCompanyName() {

    return 'Necesita ingresar el nombre de su empresa';


  }
  getErrorMessageRuc() {

    if (this.registerForm.get('ruc')?.hasError('required')) {
      console.log('entro a requerido')
      return 'Necesita ingresar el RUC';
    }
    if (this.registerForm.get('ruc')?.hasError('minlength')) {
      console.log('entro a minimo')
      return 'El RUC tiene menos de 13 caracteres';
    }
    if (this.registerForm.get('ruc')?.hasError('maxlength')) {
      return 'El RUC tiene mas de 13 caracteres';
    }
    return this.registerForm.get('ruc')?.hasError('pattern') ? 'No es un RUC valido' : '';
  }
  onSubmit() {
    if (this.registerForm.invalid) {
      return;
    }
    this.authService.register(this.registerForm.value).
      subscribe(
        data => {
          const dialogRef = this.dialog.open(DialogAlertaComponent, {

            data: { message: 'Usuario creado correctamente' }
          });
          this.router.navigate(['login'])
        },
        error => {
          const dialogRef = this.dialog.open(DialogAlertaComponent, {

            data: { message: error.error.message }
          });

        }
      )
  }
}
